Modulo de upload de imagens


Funcionalidade adicinada para chamar dentro de um CMS

**Steps:**

**1º** - criar um novo banner fazer os uploads das imagens e banner;

**2º** - dentro da Pagina CMS, na aba "design" adicionar o modelo: 


```
#!php

<reference name="content">
<block type="allmnbanners/banners" name="allmnbanner">
<action method="setData"><name>identifier</name><value>lookbook</value></action>
 </block>
</reference>
```


Nesse exemplo, o "identifier" esta como lookbook, para configurar, basta mudar para o novo identifier.



## HOW TO USE - INSTALL/UPDATE

- Create file *composer.json* with this sample content:

```
#!php


{
    "name": "magento/store",
    "require": {
        "magento-hackathon/magento-composer-installer": "*",
        "allmn/banners": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:kistters/allmn_banners.git"
        }
    ],
    "extra": {
        "magento-root-dir": ".",
        "magento-deploystrategy": "copy",
        "magento-force": true
    }
}
```

- run "composer update allmn/banners"
- Use git to commit files and use after this step the normal process workflow