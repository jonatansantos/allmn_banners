<?php
/**
 * Magento
 */


/**
 */
class Allmn_Banners_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     *
     */
    public function indexAction()
    {
        $this->loadLayout();
         
        $block = $this->getLayout()
        ->createBlock('allmnbanners/banners');
        $block->setData('identifier','campanha');
         
        $this->getLayout()->getBlock('content')->append($block);
         
        //Release layout stream... lol... sounds fancy
        $this->renderLayout();
    }


    /* adicionar via xml, dentro do CMS
    * 
    <reference name="content">
        <block type="allmnbanners/banners" name="allmnbanner">
            <action method="setData"><name>identifier</name><value>lookbook</value></action>
        </block>
    </reference>
    *
    */
}