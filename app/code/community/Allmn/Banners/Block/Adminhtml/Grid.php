<?php
/*
 * 
 */
class Allmn_Banners_Block_Adminhtml_Grid extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	
	public function __construct(){
		$this->_controller = 'adminhtml_banners';
		$this->_blockGroup = 'allmnbanners';
		$this->_headerText = 'Banners';
		$this->_addButtonLabel = 'Criar Slider';

		parent::__construct();
	}
}
?>